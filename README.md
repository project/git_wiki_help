## INTRODUCTION

This module will allow to use a cloned wiki from gitlab to your site,
as help pages. Similar to topics in help pages but with its own section named
Git wiki help.

It will not perform any fetch, checkout or sync using git. So the cloning
and updating must be done manually.

It will detect the markdown files of the directory defined in settings, and will
turn to managed file the possible images found usually in wiki uploads directory.


## REQUIREMENTS

- Composer and info will install the dependencies. But it will use the great
https://commonmark.thephpleague.com/ markdown library from php league.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
So first define the directory where the wiki will be found, if left blank, git_wiki_help
will be used. So cloning the git wiki must be done in this directory.

The html filter format will be the one that will be used after converting from markdown
using the excellent package from phpleague https://commonmark.thephpleague.com/ . Take
care that this text format allows all the html elements and properties that a markdown
may use.

Origin of Wiki repository will be used to add the original link reference at the footer
of each wiki page.

If you visit the page that obtains any removed image will make this as managed in Drupal
managed files, setting this image to permanent.
The final check (Allow clearing all the files already created) in settings will be used
to set all the permanent managed images found while rendering the pages back to
temporary. Cron will delete those after a while (6 hours or so).

There's a twig template to present the wiki pages as three columns:

Table of contents of the page|   Content  of the page |   Other pages index


## MAINTAINERS

Current maintainers for Drupal 10:

- Aleix Quintana Alsius (aleix) - https://www.drupal.org/u/aleix

While working in communia.org
