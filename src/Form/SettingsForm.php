<?php

declare(strict_types=1);

namespace Drupal\git_wiki_help\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\Core\Url;

/**
 * Configure Git wiki help settings for this site.
 */
final class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'git_wiki_help_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['git_wiki_help.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $files_stream = PrivateStream::basePath() ?: PublicStream::basePath();
    $form['file_private_path'] = [
      '#type' => 'item',
      '#title' => $this->t('Git file system path'),
      '#markup' => (PrivateStream::basePath()) ?
      $this->t('Private files set. Wiki directory will use private files.') : $this->t('Private files not set. Wiki directory should be in public directory. (This wiki will be publicly accessible!)'),
      '#description' => $this->t('Set a directory in @files_stream file system path to get wiki pages. If not set git_wiki_help directory will be used.', ['@files_stream' => $files_stream]),
    ];
    $form['git_wiki_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location of Wiki repository'),
      '#default_value' => $this->config('git_wiki_help.settings')->get('git_wiki_location'),
    ];

    $form['filter_format'] = [
      '#type' => 'select',
      '#options' => $this->getFilterFormatOptions(),
      '#title' => $this->t('HTML filter format'),
      '#description' => $this->t('The filter format to use for filtering the markdown content. Further selectable filter formats can be created on the <a href=@link>Text formats and editors</a> page.', ['@link' => Url::fromRoute('filter.admin_overview')->toString()]),
      '#default_value' => $this->config('git_wiki_help.settings')->get('filter_format'),
      '#required' => TRUE,
    ];
    $form['git_wiki_origin_location'] = [
      '#type' => 'url',
      '#title' => $this->t('Origin of Wiki repository'),
      '#default_value' => $this->config('git_wiki_help.settings')->get('git_wiki_origin_location'),
    ];
    $form['set_files_temporary'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Allow clearing all the files already created'),
      '#description' => $this->t("Will mark all managed files (matching with pattern git-wiki-help-*.png and path defined to what's in git wiki location setting) as temporary. Cron will remove these files later. This checkbox will perform this action once and will always remain unchecked."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * Retrieves all enabled filter formats as an options array.
   */
  protected function getFilterFormatOptions() {
    $formats = filter_formats();
    $options = [];
    foreach ($formats as $format) {
      $options[$format->id()] = $format->label();
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // @todo Validate the form here.
    // Example:
    // @code
    //   if ($form_state->getValue('example') === 'wrong') {
    //     $form_state->setErrorByName(
    //       'message',
    //       $this->t('The value is not correct.'),
    //     );
    //   }
    // @endcode
    parent::validateForm($form, $form_state);
    $stream_base = PrivateStream::basePath() ?: PublicStream::basePath();
    $loc = $stream_base . "/" . $form_state->getValue('git_wiki_location');
    // Set not defined to default to prevent scandir of root files dir.
    if ($form_state->getValue('git_wiki_location') === "") {
      $loc = $stream_base . "/git_wiki_help/";
    }

    if (!file_exists($loc)) {
      $form_state->setErrorByName(
        'git_wiki_location',
        $this->t('Location not found'),
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('git_wiki_help.settings')
      ->set('filter_format', $form_state->getValue('filter_format'))
      ->set('git_wiki_origin_location', $form_state->getValue('git_wiki_origin_location'))
      ->save();
    if ($form_state->getValue('git_wiki_location') !== "") {
      // Only set if not defined "" to prevent scandir of root files dir.
      $this
        ->config('git_wiki_help.settings')
        ->set('git_wiki_location', $form_state->getValue('git_wiki_location'))
        ->save();
    }

    parent::submitForm($form, $form_state);
    if ($form_state->getValue('set_files_temporary')) {
      $batch = [
        'title' => t('Set git wiki help files as temporary'),
        'operations' => [
          ['git_wiki_help_set_git_wiki_help_files_temporary', []],
        ],
        'finished' => 'git_wiki_help_files_temporary_finished',
      ];
      batch_set($batch);
    }
  }

}
