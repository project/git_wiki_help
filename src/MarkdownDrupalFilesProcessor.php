<?php

namespace Drupal\git_wiki_help;

use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\file\FileRepositoryInterface;
use League\CommonMark\Environment\EnvironmentInterface;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Extension\CommonMark\Node\Inline\Image;

/**
 * Processes the files to exist in drupal files table.
 */
class MarkdownDrupalFilesProcessor {

  /**
   * The markdown processor environment.
   *
   * @var \League\CommonMark\Environment\EnvironmentInterface
   */
  private $environment;

  /**
   * Generates file URLs for a stream to an external or local file.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  private $fileUrlGenerator;

  /**
   * The base to use when generating the url to images.
   *
   * @var string
   */
  private $streamBase;

  /**
   * The file repository service.
   *
   * @var \Drupal\file\FileRepositoryInterface
   */
  protected $fileRepository;

  /**
   * Contructor of files served by drupal.
   *
   * @param \League\CommonMark\Environment\EnvironmentInterface $environment
   *   The commonmark processor environment.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file url generator service.
   * @param \Drupal\file\FileRepositoryInterface $file_repository
   *   The file repository service.
   * @param string $stream_base
   *   The base to use when generating the url to images.
   */
  public function __construct(EnvironmentInterface $environment, FileUrlGeneratorInterface $file_url_generator, FileRepositoryInterface $file_repository, $stream_base) {
    $this->environment = $environment;
    $this->fileUrlGenerator = $file_url_generator;
    $this->fileRepository = $file_repository;
    $this->streamBase = $stream_base;
  }

  /**
   * Creates the file as drupal file entity.
   *
   * @param \League\CommonMark\Event\DocumentParsedEvent $event
   *   The document parsed event.
   */
  public function onDocumentParsed(DocumentParsedEvent $event): void {
    $document = $event->getDocument();
    $walker = $document->walker();
    while ($event = $walker->next()) {
      $node = $event->getNode();

      // Only stop at Link nodes when we first encounter them.
      if (!($node instanceof Image) || !$event->isEntering()) {
        continue;
      }
      $url = $node->getUrl();
      if ($this->isUrlInternal($url)) {
        $stream_url = $this->streamBase . "/$url";

        if (!($image = $this->fileRepository->loadByUri($stream_url))) {
          $filename = "git-wiki-help-" . basename(dirname($stream_url)) . "-" . basename($stream_url);
          $image = File::create([
            'filename' => $filename,
            'uri' => $stream_url,
            'uid' => 1,
            'status' => FileInterface::STATUS_PERMANENT,
          ]);
          $image->save();
        }
        if ($image->status !== FileInterface::STATUS_PERMANENT) {
          $image->status = FileInterface::STATUS_PERMANENT;
          $image->save();
        }

        $generated_url = $this->fileUrlGenerator->generateString($stream_url);
        $node->setUrl($generated_url);
        $node->data->append('attributes/class', 'inside-image');
      }
    }
  }

  /**
   * Checks if the url is internal or not.
   *
   * @param string $url
   *   The url to check.
   *
   * @return bool
   *   If is internal or not.
   */
  private function isUrlInternal(string $url): bool {
    return (!preg_match('/^https?:\/\//', $url));
  }

}
