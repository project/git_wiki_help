<?php

namespace Drupal\git_wiki_help;

use League\CommonMark\Environment\EnvironmentInterface;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Event\DocumentRenderedEvent;
use League\CommonMark\Extension\CommonMark\Node\Block\ListBlock;
use League\CommonMark\Node\Block\Paragraph;
use League\CommonMark\Node\Inline\Text;
use League\CommonMark\Output\RenderedContent;

/**
 * Processes the split of table of contents of content.
 */
class MarkdownExtractTOCProcessor {
  /**
   * The markdown conversion environment.
   *
   * @var League\CommonMark\Environment\EnvironmentInterface environment
   */
  private $environment;

  /**
   * Processor constructor.
   *
   * @param \League\CommonMark\Environment\EnvironmentInterface $environment
   *   The markdown conversion environment.
   */
  public function __construct(EnvironmentInterface $environment) {
    $this->environment = $environment;
  }

  /**
   * Adds specific placeholder to let split in render phase.
   *
   * @param \League\CommonMark\Event\DocumentParsedEvent $event
   *   The document parsed event.
   */
  public function onDocumentParsed(DocumentParsedEvent $event): void {
    $document = $event->getDocument();
    $walker = $document->walker();
    while ($event = $walker->next()) {
      $node = $event->getNode();

      // Only stop at ListBlock when we first encounter them.
      if (!(($node instanceof ListBlock) && $node->data->has('attributes/class')) || !$event->isEntering()) {
        continue;
      }
      if (!$node->data->get('attributes/class') === "table-of-contents") {
        continue;
      }
      $text = new Text('****END_OF_TABLE_OF_CONTENT****');
      $p = new Paragraph();
      $p->data->append('attributes/data-toc-separator', 'end-of-toc');
      $p->appendChild($text);

      $node->insertAfter($p);
      $document->data->set('table_of_contents', $node);
    }
  }

  /**
   * Splits the contents based on placeholder placement.
   *
   * @param \League\CommonMark\Event\DocumentRenderedEvent $event
   *   The document rendered event.
   */
  public function onDocumentRendered(DocumentRenderedEvent $event): void {
    if ($event->getOutput()->getDocument()->data->get('table_of_contents', NULL) === NULL) {
      return;
    }
    $split_output = explode(
      '<p data-toc-separator="end-of-toc">****END_OF_TABLE_OF_CONTENT****</p>',
      $event->getOutput());
    if (count($split_output) > 0) {
      $event->replaceOutput(new RenderedContent(
        $event->getOutput()->getDocument(),
        "<div class='table-of-contents-wrapper'>{$split_output[0]}</div><div class='content-wrapper'>{$split_output[1]}</div>"
      ));
    }
  }

}
