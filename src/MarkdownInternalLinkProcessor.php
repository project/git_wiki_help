<?php

namespace Drupal\git_wiki_help;

use League\CommonMark\Environment\EnvironmentInterface;
use League\CommonMark\Event\DocumentParsedEvent;
use League\CommonMark\Extension\CommonMark\Node\Inline\Link;

/**
 * Processes the internal links to match git_wiki_help drupal module.
 */
class MarkdownInternalLinkProcessor {
  /**
   * The markdown conversion environment.
   *
   * @var League\CommonMark\Environment\EnvironmentInterface environment
   */
  private $environment;

  /**
   * Processor constructor.
   *
   * @param \League\CommonMark\Environment\EnvironmentInterface $environment
   *   The markdown conversion environment.
   */
  public function __construct(EnvironmentInterface $environment) {
    $this->environment = $environment;
  }

  /**
   * Set the url to proper drupal url.
   *
   * @param \League\CommonMark\Event\DocumentParsedEvent $event
   *   The document parsed event.
   */
  public function onDocumentParsed(DocumentParsedEvent $event): void {
    $document = $event->getDocument();
    $walker = $document->walker();
    while ($event = $walker->next()) {
      $node = $event->getNode();

      // Only stop at Link nodes when we first encounter them.
      if (!($node instanceof Link) || !$event->isEntering()) {
        continue;
      }

      $url = $node->getUrl();
      if ($this->isUrlInternal($url)) {
        print_r($url);
        $url_basename = substr($url, 0, -3);

        $node->setUrl("/admin/help/git_wiki_help?wiki_page=$url_basename");
        $node->data->append('attributes/class', 'external-link');
      }
    }
  }

  /**
   * Checks if the url is internal or not.
   *
   * @param string $url
   *   The url to check.
   *
   * @return bool
   *   If is internal or not.
   */
  private function isUrlInternal(string $url): bool {
    return (!preg_match('/^https?:\/\//', $url));
  }

}
