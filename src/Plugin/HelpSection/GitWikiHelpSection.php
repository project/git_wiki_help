<?php

namespace Drupal\git_wiki_help\Plugin\HelpSection;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\help\Plugin\HelpSection\HelpSectionPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the git wiki list section for the help page.
 *
 * @HelpSection(
 *   id = "git_wiki_help",
 *   title = @Translation("Git help overviews"),
 *   description = @Translation("Overview from git wiki topics."),
 * )
 */
final class GitWikiHelpSection extends HelpSectionPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function listTopics() {
    $topics = git_wiki_help_append_sidebar();
    return $topics;
  }

}
