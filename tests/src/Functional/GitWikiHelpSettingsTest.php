<?php

declare(strict_types=1);

namespace Drupal\Tests\git_wiki_help\Functional;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\Core\StreamWrapper\PublicStream;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Tests\BrowserTestBase;

/**
 * Test description.
 *
 * @group git_wiki_help
 */
final class GitWikiHelpSettingsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Permissions to grant admin user.
   *
   * @var array
   */
  protected $permissions = [
    'view git wiki help pages',
    'access help pages',
    'access administration pages',
    'administer filters',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['git_wiki_help', 'help', 'filter', 'file'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $filtered_html = $this->createHtmlFilter();
    $this->permissions[] = $filtered_html->getPermissionName();
    $this->adminUser = $this->drupalCreateUser($this->permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test settings link exists.
   */
  public function testSettingsLink(): void {
    $admin_user = $this->drupalCreateUser(['access administration pages', 'administer site configuration']);
    $this->drupalLogin($admin_user);
    $this->drupalGet('admin/config/system');
    $this->assertSession()->elementExists('xpath', '//a[text() = "Git wiki help settings"]');
  }

  /**
   * Puts the wiki from fixture to defined directory to perform wiki checks.
   *
   * @param string $streamwrapper
   *   The streamwrapper path to put the wiki. ( eg. PublicStream::basePath() )
   * @param string $dest_directory
   *   Ther directory where to put the wiki.
   */
  private function putWikiGitWikiLocation($streamwrapper, $dest_directory) {
    $this
      ->config('git_wiki_help.settings')
      ->set('filter_format', 'filtered_html')
      ->set('git_wiki_origin_location', 'example.com')
      ->save();
    if (!empty($dest_directory)) {
      // Only set if not defined "" to prevent scandir of root files dir.
      $this
        ->config('git_wiki_help.settings')
        ->set('git_wiki_location', $dest_directory)
        ->save();
    }
    // Copy testwiki files from fixtures.
    $module_path = $this->container->get('extension.list.module')->getPath('git_wiki_help');
    $files_path = $module_path . '/tests/fixtures/files';
    /** @var \Drupal\Core\File\FileSystemInterface $file_system */
    $file_system = \Drupal::service('file_system');
    $git_wiki_location = \Drupal::config('git_wiki_help.settings')->get('git_wiki_location');
    $destination = $git_wiki_location ? $streamwrapper . '/' . $git_wiki_location : $streamwrapper . '/git_wiki_help';
    $destination_images = $destination . "/uploads/1245678";
    $destination_uploads = $destination . "/uploads";
    $file_system->prepareDirectory($destination_images, FileSystemInterface::CREATE_DIRECTORY);
    $files = $file_system->scanDirectory($files_path, "/.*\.md/");
    foreach ($files as $file) {
      $file_system->copy($file->uri, $destination);
    }
    $images = $file_system->scanDirectory($files_path, "/.png/");
    foreach ($images as $image) {
      $file_system->copy($image->uri, $destination_images);
    }
  }

  /**
   * Create a suitable basic filter format to render markdown.
   *
   * @return \Drupal\filter\Entity\FilterFormat
   *   The FilterFormat created.
   */
  private function createHtmlFilter() {
    // Setup Filtered HTML text format.
    $filtered_html_format = FilterFormat::create([
      'format' => 'filtered_html',
      'name' => 'Filtered HTML',
      'filters' => [
        'filter_html' => [
          'status' => 1,
          'settings' => [
            'allowed_html' => '<a href id> <p> <em> <strong> <cite> <blockquote> <code> <ul> <ol> <li> <dl> <dt> <dd> <br> <h1> <h2> <h3 id class> <img src> <div class>',
          ],
        ],
        'filter_autop' => [
          'status' => 1,
        ],
      ],
    ]);
    $filtered_html_format->save();
    return $filtered_html_format;
  }

  /**
   * Tests undefined wiki.
   */
  public function testUndefinedWiki() {
    $this->drupalGet('/admin/help');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains("Git help overviews");
    $no_content = "/<h2>Git help overviews<\/h2>\n\s*<p>Overview from git wiki topics\.<\/p>\n\s*<p>There is currently nothing in this section\.<\/p>/";
    $this->assertSession()->responseMatches($no_content);
    $this->drupalGet('/admin/help/git_wiki_help');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseMatches("@<h1>Not found</h1>@");
    $this->assertSession()->pageTextContains("Sorry, this page hasn't been created yet.");
  }

  /**
   * Tests if public file is always accessible.
   *
   * @dataProvider dataProvider
   */
  public function testLocationWiki($streamwrapper, $wiki_directory_destination) {
    if ($streamwrapper == 'private') {
      $streamwrapper = PrivateStream::basePath();
    }
    if ($streamwrapper == 'public') {
      $streamwrapper = PublicStream::basePath();
      // Remove the private path, rebuild the container and verify that private
      // can no longer be selected in the UI.
      $settings['settings']['file_private_path'] = (object) [
        'value' => '',
        'required' => TRUE,
      ];
      $this->writeSettings($settings);
      $this->rebuildContainer();
    }
    $this->putWikiGitWikiLocation($streamwrapper, $wiki_directory_destination);
    // Provide default if empty config:
    $git_wiki_location = $this->config('git_wiki_help.settings')->get('git_wiki_location') ?: "git_wiki_help";
    $this->drupalGet('/admin/help');
    $this->assertSession()->statusCodeEquals(200);
    $this->drupalGet('/admin/help/git_wiki_help');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('This is a test wiki');
    $this->assertSession()->pageTextContains('Other pages');
    $this->drupalGet('/admin/help/git_wiki_help', ['query' => ['wiki_page' => 'other_page']]);
    // Contains original content sidebar table of contents image.
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('This is another page in wiki linked from');
    // Contains table of contents.
    $this->assertSession()->responseMatches('@<div class="aside-col toc">\n<ul>\n<li>\n<a href="#content-the-other-page">The other page</a>@');

    if ($streamwrapper == 'private') {
      $this->assertSession()->responseMatches('@<img src="/system/files/' . $git_wiki_location . '/uploads/12345678/image\.png">@');
    }
    if ($streamwrapper == 'public') {
      // @todo get simpletest ID.
      $this->assertSession()->responseMatches('@<img src="/sites/simpletest/[0-9]*/files/' . $git_wiki_location . '/uploads/12345678/image\.png">@');
    }
    $this->assertSession()->pageTextContains('Other pages');
  }

  /**
   * Tests wiki in public while private is enabled.
   */
  public function testPublicWhilePrivateActive() {
    // Set as public while private streamwrapper comes enabled so it must fail.
    $this->putWikiGitWikiLocation(PublicStream::basePath(), "testwiki");
    $this->drupalGet('/admin/help');
    $this->assertSession()->statusCodeEquals(200);
    $no_content = "/<h2>Git help overviews<\/h2>\n\s*<p>Overview from git wiki topics\.<\/p>\n\s*<p>There is currently nothing in this section\.<\/p>/";
    $this->assertSession()->responseMatches($no_content);

    $this->drupalGet('/admin/help/git_wiki_help');
    $this->assertSession()->statusCodeEquals(200);
    $not_found = "/<h1>Not found<\/h1>/";
    $this->assertSession()->responseMatches($not_found);
    $this->drupalGet('/admin/config/content/formats');
    $this->drupalGet('/admin/config/content/formats/manage/filtered_html');
  }

  /**
   * Data provider for various tests.
   */
  public function dataProvider():array {
    // To provide streamwrapper, wiki_directory_destination.
    return [
      ["private", ""],
      ["private", "testwiki"],
      ["public", ""],
      ["public", "testwiki"],
    ];
  }

}
